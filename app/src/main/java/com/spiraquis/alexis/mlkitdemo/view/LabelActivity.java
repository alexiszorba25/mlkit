package com.spiraquis.alexis.mlkitdemo.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.ml.vision.FirebaseVision;
import com.google.firebase.ml.vision.cloud.FirebaseVisionCloudDetectorOptions;
import com.google.firebase.ml.vision.cloud.label.FirebaseVisionCloudLabel;
import com.google.firebase.ml.vision.cloud.label.FirebaseVisionCloudLabelDetector;
import com.google.firebase.ml.vision.common.FirebaseVisionImage;
import com.google.firebase.ml.vision.label.FirebaseVisionLabel;
import com.google.firebase.ml.vision.label.FirebaseVisionLabelDetector;
import com.google.firebase.ml.vision.label.FirebaseVisionLabelDetectorOptions;
import com.spiraquis.alexis.mlkitdemo.R;
import com.spiraquis.alexis.mlkitdemo.utils.ImageChooser;

import java.util.List;
import java.util.Locale;

import static com.spiraquis.alexis.mlkitdemo.utils.ImageUtils.handleSamplingAndRotationBitmap;

public class LabelActivity extends AppCompatActivity {
    private static final int PICK_FROM_FILECAMERA = 1;
    Button btnImage;
    private Uri mImageCaptureUri;
    ImageView imgView;
    TextView txtMLKitLabels;
    TextView txtMLKitLabelsCloud;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_label);
        imgView = findViewById(R.id.imgView);
        txtMLKitLabels = findViewById(R.id.txtMlKit);
        txtMLKitLabelsCloud = findViewById(R.id.txtMlKitCloud);
        btnImage = findViewById(R.id.btnImage1);
        FirebaseApp.initializeApp(this);
        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtMLKitLabels.setVisibility(View.GONE);
                txtMLKitLabelsCloud.setVisibility(View.GONE);
                mImageCaptureUri = ImageChooser.getUriFromCamera(LabelActivity.this);
                startActivityForResult(ImageChooser.getPickImageChooserIntent(LabelActivity.this), PICK_FROM_FILECAMERA);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Bundle extras;
        if (resultCode != RESULT_OK) {
            Log.d("image", "onActivityResult: Error="+resultCode);
            Log.d("image", "onActivityResult requestcode: "+requestCode);
            if (data!=null) {
                Log.d("image", "onActivityResult data: " + data.getExtras());
            }else{
                Log.d("image", "onActivityResult data: null");
            }
            return;
        }

        switch (requestCode) {
            case PICK_FROM_FILECAMERA:

                Uri mImageCropUri = getPickImageResultUri(data);
                try {
                    Bitmap imgBitmap = handleSamplingAndRotationBitmap(LabelActivity.this, mImageCropUri);
                    imgView.setImageBitmap(imgBitmap);
                    FirebaseVisionImage image = FirebaseVisionImage.fromBitmap(imgBitmap);
                    processOnDeviceLabelModel(image);
                    processCloudLabelModel(image);
                }catch(Exception e){
                    Log.d("image", "onActivityResult: "+e.getMessage());
                }
                //imgView.setImageURI(mImageCropUri);
                Log.d("image", "onActivityResult: mImageCaptureUri="+mImageCropUri);

                break;

        }
    }
    public Uri getPickImageResultUri(Intent  data) {
        boolean isCamera = true;
        String action;
        if (data != null && data.getData() != null) {
            action = data.getAction();
            isCamera = action != null  && action.equals(MediaStore.ACTION_IMAGE_CAPTURE);
        }
        return isCamera ?  mImageCaptureUri : data.getData();
    }
    public void processOnDeviceLabelModel(FirebaseVisionImage image){
        FirebaseVisionLabelDetectorOptions options =
                new FirebaseVisionLabelDetectorOptions.Builder()
                        .setConfidenceThreshold(Float.parseFloat(getString(R.string.mlkit_min_confidence)))
                        .build();
        FirebaseVisionLabelDetector detector = FirebaseVision.getInstance()
                .getVisionLabelDetector(options);
        Task<List<FirebaseVisionLabel>> result =
                detector.detectInImage(image)
                        .addOnSuccessListener(
                                new OnSuccessListener<List<FirebaseVisionLabel>>() {
                                    @Override
                                    public void onSuccess(List<FirebaseVisionLabel> labels) {
                                        StringBuilder txtLabels= new StringBuilder();
                                        Log.d("image", "onSuccess: MLKIT success");
                                        for (FirebaseVisionLabel label: labels) {
                                            String text = label.getLabel();
                                            String entityId = label.getEntityId();
                                            float confidence = label.getConfidence();
                                            txtLabels.append(text).append(": ").append(String.format(Locale.getDefault(), "%.2f", confidence)).append("\n");
                                            Log.d("image", "onSuccess: MLKIT: "+txtLabels);
                                        }
                                        if (!txtLabels.toString().equals("")) {
                                            txtMLKitLabels.setText(txtLabels.toString());
                                        }else{
                                            txtMLKitLabels.setText(R.string.no_labels);
                                        }
                                        txtMLKitLabels.setVisibility(View.VISIBLE);
                                    }
                                })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d("image", "onFailure: MLKIT "+e.getMessage());
                                    }
                                });

    }
    public void processCloudLabelModel(FirebaseVisionImage image){
        FirebaseVisionCloudDetectorOptions options =
                new FirebaseVisionCloudDetectorOptions.Builder()
                        //.setConfidenceThreshold(Float.parseFloat(getString(R.string.mlkit_min_confidence)))
                        .setMaxResults(10)
                        .setModelType(FirebaseVisionCloudDetectorOptions.LATEST_MODEL)
                        .build();
        FirebaseVisionCloudLabelDetector detector = FirebaseVision.getInstance()
                .getVisionCloudLabelDetector(options);
        Task<List<FirebaseVisionCloudLabel>> result =
                detector.detectInImage(image)
                        .addOnSuccessListener(
                                new OnSuccessListener<List<FirebaseVisionCloudLabel>>() {
                                    @Override
                                    public void onSuccess(List<FirebaseVisionCloudLabel> labels) {
                                        StringBuilder txtLabels= new StringBuilder();
                                        Log.d("image", "onSuccess: MLKIT success");
                                        for (FirebaseVisionCloudLabel label: labels) {
                                            String text = label.getLabel();
                                            String entityId = label.getEntityId();
                                            float confidence = label.getConfidence();
                                            txtLabels.append(text).append(": ").append(String.format(Locale.getDefault(), "%.2f", confidence)).append("\n");
                                            Log.d("image", "onSuccess: MLKIT: "+txtLabels);
                                        }
                                        if (!txtLabels.toString().equals("")) {
                                            txtMLKitLabelsCloud.setText(txtLabels.toString());
                                        }else{
                                            txtMLKitLabelsCloud.setText(R.string.no_labels);
                                        }
                                        txtMLKitLabelsCloud.setVisibility(View.VISIBLE);
                                    }
                                })
                        .addOnFailureListener(
                                new OnFailureListener() {
                                    @Override
                                    public void onFailure(@NonNull Exception e) {
                                        Log.d("image", "onFailure: MLKIT "+e.getMessage());
                                    }
                                });

    }
}
