package com.spiraquis.alexis.mlkitdemo.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.spiraquis.alexis.mlkitdemo.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button btnLabel = findViewById(R.id.btn1);
        Button btnLive = findViewById(R.id.btn2);
        btnLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LabelActivity.class);
                startActivity(intent);
            }
        });
        btnLive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this,LivePreviewActivity.class);
                startActivity(intent);
            }
        });

    }
}
