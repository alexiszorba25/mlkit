package com.spiraquis.alexis.mlkitdemo.utils;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.widget.Toast;

import com.spiraquis.alexis.mlkitdemo.BuildConfig;
import com.spiraquis.alexis.mlkitdemo.R;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class ImageChooser {
    private static Uri mImageCaptureUri;
    public static Uri getUriFromCamera(Context contexto){
        try{
            Log.d("image", "getPickImageChooserIntent: contexto="+contexto);
            mImageCaptureUri = FileProvider.getUriForFile(contexto,
                    BuildConfig.APPLICATION_ID + ".provider",
                    createImageFile(contexto));
        } catch(ActivityNotFoundException anfe){
            //display an error message
            Toast.makeText(contexto, R.string.error_no_camera, Toast.LENGTH_LONG).show();
        }catch (IOException e) {
            Log.d("image", "onClick: Error io +"+e.getMessage());
            Toast.makeText(contexto, R.string.error_camera, Toast.LENGTH_LONG).show();
            //return;
        }catch (Exception e) {
            Log.d("image", "onClick: Error else +"+e.getMessage());
            Toast.makeText(contexto, R.string.error_camera, Toast.LENGTH_LONG).show();
            //return;
        }
        return mImageCaptureUri;
    }
    public static Intent getPickImageChooserIntent(Context contexto) {



        List<Intent> allIntents = new ArrayList<>();
        PackageManager packageManager =  contexto.getPackageManager();

// collect all camera intents
        Intent captureIntent = new  Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        List<ResolveInfo> listCam =  packageManager.queryIntentActivities(captureIntent, 0);
        for (ResolveInfo res : listCam) {
            Intent intent = new  Intent(captureIntent);
            intent.setType("image/jpeg");
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            if (mImageCaptureUri != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            }
            allIntents.add(intent);
        }

// collect all gallery intents
        Intent galleryIntent;
        galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        List<ResolveInfo> listGallery =  packageManager.queryIntentActivities(galleryIntent, 0);
        for (ResolveInfo res : listGallery) {
            Intent intent = new  Intent(galleryIntent);
            intent.setComponent(new  ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(res.activityInfo.packageName);
            allIntents.add(intent);
        }
// the main intent is the last in the  list (fucking android) so pickup the useless one
        Intent mainIntent =  allIntents.get(allIntents.size() - 1);
        for (Intent intent : allIntents) {
            if  (intent.getComponent().getClassName().equals("com.android.documentsui.DocumentsActivity"))  {
                mainIntent = intent;
                break;
            }
        }
        allIntents.remove(mainIntent);

// Create a chooser from the main  intent
        Intent chooserIntent =  Intent.createChooser(mainIntent, contexto.getString(R.string.complete_action));

// Add all other intents
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS,  allIntents.toArray(new Parcelable[allIntents.size()]));

        return chooserIntent;
    }

    private static File createImageFile(Context contexto) throws IOException {
        String mCurrentPhotoPath;
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = contexto.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        Log.d("image", "createImageFile: storageDir="+storageDir);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );
        Log.d("image", "createImageFile: storageDir2="+storageDir);
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        Log.d("image", "createImageFile: mCurrentPhotoPath="+mCurrentPhotoPath);
        return image;
    }
}
