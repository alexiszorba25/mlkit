# MLKit

Demo App en Android del MLKit de Machine Learning de Google utilizada en la Meetup de Android Devs Buenos Aires el d�a 7 de Agosto de 2018.
Se muestran las caracter�sticas de Image labeling tanto On-Device como Cloud y Face detection.
https://meetup.com/es-ES/Android-Devs-Buenos-Aires/events/253170021

# Setup

Para poder ejecutar la App, debes habilitar MLKit en tu cuenta de Firebase, asociar el proyecto, habilitar Vision Api en Google Cloud, pasar a un plan de facturaci�n Blaze y agregar tu propio archivo google-services.json en la carpeta app seg�n los datos de la configuraci�n de tu cuenta en Firebase.